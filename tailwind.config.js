const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'xs': '380px',
      ...defaultTheme.screens,
    },
    extend: {
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
