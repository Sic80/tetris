import { GameBoardState } from './types/GameBoardState';
import {RotationPatterns} from './types/RotationPatternsType'


export default function RotateTetromino(gameBoardState: GameBoardState, input: string): GameBoardState {
      
    const { tetromino, gameBoard } = gameBoardState;
    const { type, position, id } = tetromino;
    const positionDiff = position[0] - position[1];
    const rotationCount = positionDiff === -1 ? 0 : positionDiff === 10 ? 1 : positionDiff === 1 ? 2 : 3;  
    let gameBoardCopy: (number | string)[] = [...gameBoard]
    let gameBoardStateCopy = {tetromino: {type: type, position: [...position], id: id}, gameBoard: [...gameBoard]}    
    const anticlockwisePattern: RotationPatterns = {
      i: [[11, 0, -11, -22], [-9, 0, 9, 18], [-11, 0, 11, 22], [9, 0, -9, -18]],
      o: [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
      t: [[11, 0, -11, -9], [-9, 0, 9, -11], [-11, 0, 11, 9], [9, 0, -9, 11]],
      j: [[11, 0, -11, -20], [-9, 0, 9, -2], [-11, 0, 11, 20], [9, 0, -9, 2]],
      l: [[11, 0, -11, 2], [-9, 0, 9, -20], [-11, 0, 11, -2], [9, 0, -9, 20]],
      s: [[0, -11, 2, -9], [0, 9, -20, -11], [0, 11, -2, 9], [0, -9, 20, 11]],
      z: [[11, 0, -9, -20], [-9, 0, -11, -2], [-11, 0, 9, 20], [9, 0, 11, 2]]
    };
    const clockwisePattern: RotationPatterns = {
      i: [[-9, 0, 9, 18], [-11, 0, 11, 22], [9, 0, -9, -18], [11, 0, -11, -22]],
      o: [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
      t: [[-9, 0, 9, -11], [-11, 0, 11, 9], [9, 0, -9, 11], [11, 0, -11, -9]],
      j: [[-9, 0, 9, -2], [-11, 0, 11, 20], [9, 0, -9, 2], [11, 0, -11, -20]],
      l: [[-9, 0, 9, -20], [-11, 0, 11, -2], [9, 0, -9, 20], [11, 0, -11, 2]],
      s: [[0, 9, -20, -11], [0, 11, -2, 9],[0, -9, 20, 11], [0, -11, 2, -9]],
      z: [[-9, 0, -11, -2], [-11, 0, 9, 20], [9, 0, 11, 2], [11, 0, -9, -20]]
    };
    const clockwiseRotation = clockwisePattern[type][rotationCount].map((square, idx) => position[idx] + square);
    const anticlockwiseRotation = anticlockwisePattern[type][rotationCount].map((square, idx) => position[idx] + square);
    const rotatedPosition = input === "n" ? anticlockwiseRotation : clockwiseRotation;
    
    const gameBoardLeftEdge: number[] = Array.from(Array(220).keys()).filter(number => number % 10 === 0);
    const gameBoardRightEdge: number[] = Array.from(Array(220).keys()).filter(number => (number+1) % 10 === 0);
    const gameBoardFirstLine = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];    
    const isRotatedPositionEmpty: boolean = rotatedPosition.filter(square => !position.includes(square)).every(square => gameBoard[square] === 0);
    const isTetrominoOnTheLeftEdge: boolean = rotatedPosition.some(square => gameBoardLeftEdge.includes(square));
    const isTetrominoOnTheRightEdge: boolean = rotatedPosition.some(square => gameBoardRightEdge.includes(square));
    const isGoingThroughTheSide:boolean = isTetrominoOnTheLeftEdge && isTetrominoOnTheRightEdge;
    const isTetrominoOnThefirstLine: boolean = rotatedPosition.some(square => gameBoardFirstLine.includes(square));
    const canTetrominoRotate = isRotatedPositionEmpty && !isGoingThroughTheSide && !isTetrominoOnThefirstLine;
    
    if (canTetrominoRotate) {
        position.forEach(square => gameBoardCopy[square] = 0);
        rotatedPosition.forEach(square => gameBoardCopy[square] = type);
        gameBoardStateCopy = {tetromino: {type: type, position: [...rotatedPosition], id: id}, gameBoard: [...gameBoardCopy]}
      } else {
        gameBoardStateCopy = {tetromino: {type: type, position: [...position], id: id}, gameBoard: [...gameBoardCopy]};
      }
    
    return gameBoardStateCopy;

    }
