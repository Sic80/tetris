import { GameBoardState } from './types/GameBoardState';

export default function ClearLines(gameBoardState: GameBoardState): {gameBoardState: GameBoardState, removedLines: number} {
    const _ = require('lodash');
    const { tetromino, gameBoard } = gameBoardState;
    const newLine: number[] = new Array(10).fill(0);
    let removedLines = 0;
    let gameBoardLines: (number | string)[][] = _.chunk([...gameBoard], 10);
    const removedLinesIdx: number[] = [];
    
    gameBoardLines.forEach((line:(number | string)[], lineIdx: number) => {
        if (line.every(square => square !== 0)) {
            gameBoardLines = [[...newLine], ...gameBoardLines.slice(0, lineIdx), ...gameBoardLines.slice(lineIdx + 1)]
            removedLinesIdx.push(lineIdx);
        };
    });    
    
        if (removedLinesIdx.length > 0) {
            removedLines = removedLinesIdx.length;
        }

    return {gameBoardState: {tetromino: tetromino, gameBoard: [...gameBoardLines.flat()]}, removedLines: removedLines};

}