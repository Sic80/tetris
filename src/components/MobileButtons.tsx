import { FunctionComponent } from "react";
import { CgPlayButtonR, CgPlayPauseR } from 'react-icons/cg';


interface MobileButtonsProps {
    keyAndButtonsHandler: (e: string) => void;
}
 
const MobileButtons: FunctionComponent<MobileButtonsProps> = ({ keyAndButtonsHandler }) => {

    return ( 
        <div className="grid grid-cols-2 md:hidden justify-content-center bg-gray-500 pt-2 pb-2 xs:pt-4 xs:pb-4 border-t-2 border-b-2 border-black">
            <div className="col grid grid-cols-3">
                <CgPlayButtonR 
                    className="col h-10 w-10 xs:h-12 xs:w-12 transform rotate-180 self-center justify-self-end -mr-2 xs:-mr-1"                   
                    onClick={(e) =>{
                        keyAndButtonsHandler("ArrowLeft");
                        if (navigator.vibrate) {
                            navigator.vibrate(10);
                        }
                    }}     
                />


                <div className=" col grid place-content-center">
                    <CgPlayPauseR 
                        className="h-10 w-10 xs:h-12 xs:w-12 mb-4 xs:mb-6"
                        onClick={(e) =>{
                            keyAndButtonsHandler("p");
                            if (navigator.vibrate) {
                                navigator.vibrate(10);
                            }
                        }}   
                    />
                    <CgPlayButtonR 
                        className="h-10 w-10 xs:h-12 xs:w-12 transform rotate-90 mt-4 xs:mt-6"
                        onClick={(e) =>{
                            keyAndButtonsHandler("ArrowDown");
                            if (navigator.vibrate) {
                                navigator.vibrate(10);
                            }
                        }}    
                    />
                </div>


                <CgPlayButtonR 
                    className=" h-10 w-10 xs:h-12 xs:w-12 self-center justify-self-start -ml-2 xs:-ml-1"
                    onClick={(e) =>{
                        keyAndButtonsHandler("ArrowRight");
                        if (navigator.vibrate) {
                            navigator.vibrate(10);
                        }
                    }}                
                />
            </div>
            <div className="col grid grid-cols-2 h-full place-content-center ">
                <div 
                    className="grid col h-12 w-12 bg-red-800 border-black border-2 rounded-full justify-self-end items-center mt-10"
                    onClick={(e) =>{
                        keyAndButtonsHandler("n");
                        if (navigator.vibrate) {
                            navigator.vibrate(10);
                        }
                    }}
                >
                   <p className="font-bold">B</p>
                </div>

                <div  
                    className=" grid col h-12 w-12 bg-red-800 border-black border-2 rounded-full justify-self-center items-center"
                    onClick={(e) =>{
                        keyAndButtonsHandler("m");
                        if (navigator.vibrate) {
                            navigator.vibrate(10);
                        }
                    }}
                >
                    <p className="font-bold">A</p>
                </div>
            </div>                                
        </div>
     );
}
 
export default MobileButtons;