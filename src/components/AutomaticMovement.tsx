import { GameBoardState } from './types/GameBoardState';
import ClearLines from './ClearLines';
import { Tetromino } from './types/Tetromino';



export default function AutomaticMovement(gameBoardState: GameBoardState, gameTetrominoes: Tetromino[]): {gameBoardState: GameBoardState, removedLines: number} {
        
    const { tetromino, gameBoard } = gameBoardState;
    const { position, type, id } = tetromino;
    
    const nextPosition: number[] = position.map((square: number) => square + 10);    
    const isPositionNotEmpty: boolean = nextPosition.filter(square => !position.includes(square)).some(square => gameBoard[square] !== 0);
    
    let newTetromino;
    let gameBoardCopy;
    let removedLines: number = 0; 
    
    if (isPositionNotEmpty) {
        newTetromino = gameTetrominoes[id + 1];
        const  clearLines = ClearLines(gameBoardState);
        gameBoardCopy = [...clearLines.gameBoardState.gameBoard];
        removedLines = clearLines.removedLines;
    } else {
        gameBoardCopy = [...gameBoard];
        position.forEach((square: number) => gameBoardCopy[square] = 0);
        nextPosition.forEach((square: number) => gameBoardCopy[square] = type);
        newTetromino = {type: type, position: [...nextPosition], id: id};
    }

return {gameBoardState: {tetromino: newTetromino, gameBoard: gameBoardCopy} , removedLines: removedLines}   
}