import { FunctionComponent, useEffect, useRef, useState } from "react";
import { ActionsTypes } from "./constants/ActionsTypes";
import { PlayerRecord } from "./types/PlayerRecordInterface";

interface RecordProps {
    isGameOver: boolean;
    finalScore: number;
    topThree: PlayerRecord[];
    topThreePosition: number;
    dispatch: (e: any) => void;
}
 
const Record: FunctionComponent<RecordProps> = ({ isGameOver, finalScore, topThree, topThreePosition, dispatch }) => {

    const [inputValue, setInputValue] = useState('');
    const isInTopThree = isGameOver && (finalScore > topThree[2].score);
    const recordInputRef = useRef<HTMLInputElement>(null);
    

    useEffect(() => {
       const topThreeSaved = localStorage.getItem('topThree');
       if (topThreeSaved) {
           dispatch({type: ActionsTypes.SET_TOP_THREE, payload: JSON.parse(topThreeSaved)})
        }
    }, [dispatch])

    useEffect(() => {
        if (isInTopThree) {
            dispatch({type: ActionsTypes.REARRANGE_TOP_THREE_AND_SET_POSITION, payload: finalScore})
        }
    }, [isInTopThree, finalScore, dispatch]);
    
    useEffect(() => {
        if(recordInputRef.current) {recordInputRef.current.focus()};        
    }, [topThreePosition]);
    
    const handleKeyDown = (e: any, plIdx: number) => {
        if (e.key === "Enter") {
            const newRecord = {name: inputValue, score: finalScore};
            const topThreeCopy = [...topThree];
            topThreeCopy[plIdx] = {...newRecord};
            dispatch({type: ActionsTypes.SET_TOP_THREE, payload: topThreeCopy})
            setInputValue("");        
            localStorage.setItem('topThree', JSON.stringify(topThreeCopy))
        }
    }

return (
        <div>
            <div className="bg-black w-75 mt-2 h-auto space-y-2 text-white pt-2 pb-4 rounded-md">
                <header className="text-center mb-2">
                    RECORD
                </header>
                    {topThree.map((player, plIdx) =>
                        <div key={plIdx} className="">
                            <p className="grid grid-cols-3">
                                <span className="col-3 text-center">
                                    {plIdx + 1}{plIdx === 0 ? 'st': plIdx === 1 ? 'nd' : 'rd'}
                                </span>
                                {plIdx === topThreePosition ?
                                    (<span className="col-3 text-center">
                                        <input
                                            className='w-10 bg-black text-white text-center'
                                            value={inputValue}
                                            onChange={(e) => setInputValue(e.target.value.toUpperCase())}
                                            maxLength={3}
                                            onKeyDown={(e) => handleKeyDown(e, plIdx)}
                                            ref={plIdx === topThreePosition ? recordInputRef : null}
                                        />
                                    </span>) 
                                    : 
                                    (<span className="col-6 text-center">
                                        {player.name}
                                    </span>)
                                }
                                <span className="col-3 text-center">
                                    {player.score}
                                </span>
                            </p>
                        </div>                                 
                    )}
            </div>
        </div>

      );
}
 
export default Record;