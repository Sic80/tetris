import Main from './Main'


function App() {    

  return (
    <div className="overflow-hidden select-none">
      <Main />
    </div>
  );
}

export default App;
