import { ActionsTypes } from "./constants/ActionsTypes";
import GetGameTetrominoes from "./GetGameTetrominoes";
import { InitialState } from "./types/InitialStateInterface";
import { Action } from "./types/ReducerActionsTypes";


export function reducer(state: InitialState, action: Action): InitialState {
    const { speed, linesCounter, gameTetrominoes, gameBoardState, isGameOver, topThree, canGoOn } = state;
    const { tetromino } = gameBoardState;
    const { id } = tetromino;

    switch(action.type) {
        case ActionsTypes.SET_GAMEBOARD_STATE:
            return {...state, gameBoardState: action.payload};
        case ActionsTypes.SET_GAMEBOARD_STATE_AND_KEY_UP:
            return {...state, gameBoardState: action.payload, isKeyUp: false}
        case ActionsTypes.SET_IS_PAUSED:
            return {...state, isPaused: !state.isPaused};
        case ActionsTypes.SET_SPEED:
            return {...state, speed: speed - action.payload};
        case ActionsTypes.SET_SPEED_STEP:
            return {...state, speedStep: action.payload};
        case ActionsTypes.SET_LINES_COUNTER:
            return {...state, linesCounter: linesCounter + action.payload};
        case ActionsTypes.SET_NEXT_TETROMINO:
            return {...state, nextTetromino: gameTetrominoes[id + 1]};
        case ActionsTypes.SET_GAME_TETROMINOES:
            return {...state, gameTetrominoes: [...gameTetrominoes, ...GetGameTetrominoes(gameTetrominoes.length)]};
        case ActionsTypes.IS_GAME_OVER:
            return {...state, isGameOver: !isGameOver};
        case ActionsTypes.NEW_GAME: 
            return {...action.payload, topThree: topThree};
        case ActionsTypes.KEY_UP:
            return {...state, isKeyUp: action.payload};
        case ActionsTypes.SET_TOP_THREE:
            return {...state, topThree: action.payload, topThreePosition: -1}
        case ActionsTypes.REARRANGE_TOP_THREE_AND_SET_POSITION: 
            const { payload } = action;
            if (payload  > topThree[0].score) {
                return {...state, topThreePosition: 0, topThree: [{name: ' . . . ', score: 0}, {...topThree[0]}, {...topThree[1]}]};
            } else if (payload > topThree[1].score) {
                return {...state, topThreePosition: 1, topThree:[{...topThree[0]}, {name: ' . . . ', score: 0}, {...topThree[1]}]};
            } else if (payload > topThree[2].score){
                return {...state, topThreePosition: 2}
            } else {
                return state;
            };
        case ActionsTypes.SET_CAN_GO_ON:
            return {...state, canGoOn: !canGoOn}
        default:
            return state;
    }
}