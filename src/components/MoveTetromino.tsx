import { GameBoardState } from './types/GameBoardState';

export default function MoveTetromino(gameBoardState: GameBoardState, keyPressed?: string): GameBoardState {
    
    const { tetromino: { type, position, id }, gameBoard } = gameBoardState;
    const gameBoardLeftEdge: number[] = Array.from(Array(220).keys()).filter(number => number % 10 === 0);
    const gameBoardRightEdge: number[] = Array.from(Array(220).keys()).filter(number => (number+1) % 10 === 0);
    const isTetrominoOnTheLeftEdge: boolean = position.some(square => gameBoardLeftEdge.includes(square));
    const isTetrominoOnTheRightEdge: boolean = position.some(square => gameBoardRightEdge.includes(square));
    //const isTetrominoAtTheBottom: boolean = position.some(square => square > gameBoard.length - 10);
    
    const gameBoardCopy = [...gameBoard];
    const movedTetromino = (move: number): GameBoardState => {
        const newPosition: number[] = position.map(square => square + move);
        const isPositionEmpty = !newPosition.filter(square => !position.includes(square)).some(square => gameBoard[square] !== 0);
        if (isPositionEmpty) {
            position.forEach((square: number) => gameBoardCopy[square] = 0);
            newPosition.forEach((square: number) => gameBoardCopy[square] = type);
            return {tetromino: {type: type, position: [...newPosition], id: id}, gameBoard: [...gameBoardCopy]}
        } else {
            return gameBoardState;
        }     
    } 
    
    const directlyToTheBottom = () => {
        let move = 10;
        const isPositionEmpty = (move: number) => {
            const newPosition: number[] = position.map(square => square + move);
            return !(newPosition.filter(square => !position.includes(square)).some(square => gameBoard[square] !== 0))
        }
        while (isPositionEmpty(move)) {
            move += 10
        }
        return move - 10;                
    }
    
    switch (keyPressed) {
        case "ArrowLeft": 
            return !isTetrominoOnTheLeftEdge ? movedTetromino(-1) : gameBoardState;
        case "ArrowRight": 
            return !isTetrominoOnTheRightEdge ? movedTetromino(1) : gameBoardState;
        case "ArrowDown": 
            return movedTetromino(directlyToTheBottom());
        default:
            return {tetromino: {type: type, position: [...position], id: id}, gameBoard: [...gameBoard]};
    }
}