import { useEffect } from "react";
import { ActionsTypes } from "./constants/ActionsTypes";

export const useWindowBlur = (isPaused: boolean, isGameOver: boolean, dispatch: (e: any) => void) => {
    useEffect(() => {
        const blurHandler = () => {
            if(!isPaused && !isGameOver) {
                dispatch({type: ActionsTypes.SET_IS_PAUSED});
            }
        }
        window.addEventListener('blur', blurHandler);
        return () => {
            window.removeEventListener('blur', blurHandler);
        }
    })
}