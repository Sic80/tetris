import { FunctionComponent } from "react";

interface TableProps {
    isPaused: boolean;
    isGameOver: boolean;
    gameBoard: (string |number)[];
}
 
const Table: FunctionComponent<TableProps> = ({ isPaused, isGameOver, gameBoard}) => {


    const pause = ["P", "A", "U", "S", "E", "D"];
    const gameOver = ["G", "A", "M", "E", " ", " ", "O", "V", "E", "R"];
    const gameBoardInPause = [...new Array(102).fill(""), ...pause , ...new Array(112).fill("")];
    const gameBoardGray = gameBoard.map(square => square !== 0 ? square = "g" : square = "b");
    const gameBoardInGameOver = [...gameBoardGray.slice(0, 100), ...gameOver, ...gameBoardGray.slice(110, 210)];

    return ( 
        <table className="w-auto h-20">
            <tbody>
                {[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19, 20, 21].map((tr: number) =>
                    <tr 
                        key={tr} 
                        className="bg-black"
                        style={{display: tr > 1 ? "block" : "none"}}
                    >                          
                        {[0,1,2,3,4,5,6,7,8,9].map((td: number,) => 
                            <td 
                                key={td} 
                                className={`border border-black border-b-0 text-center ${isPaused ? "text-white text-xs" : isGameOver? `${gameBoardGray[+(tr.toString() + td)]} ${tr === 10 && 'game-over'} text-xs`: gameBoard[parseInt(tr.toString() + td)]} h-5 w-5`}
                            >
                                {isPaused && gameBoardInPause[+(tr.toString() + td)]}
                                {isGameOver && gameBoardInGameOver[+(tr.toString() + td)]}                        
                            </td>
                        )}                            
                    </tr>                    
                )} 
            </tbody>
        </table>
     );
}
 
export default Table;
