import { useEffect, useState } from "react";
import { ActionsTypes } from "./constants/ActionsTypes";
import MoveTetromino from "./MoveTetromino";
import RotateTetromino from "./RotateTetromino";
import { GameBoardState } from "./types/GameBoardState";
//ATTENTION!!!! THIS HOOK CAUSES THE OLD PROBLEM; PROBABLY DUE TO THE PRESENCE OF THE USESTATE HOOK


export const useKeyDown = (isTetrominoFalling: boolean, isPaused: boolean, gameBoardState: GameBoardState ,dispatch: (e: any) => void, input: string) => {
    //const [keyUp, setKeyUp] = useState<boolean>(true);
    useEffect(() => {
        const keys = ["ArrowLeft", "ArrowRight", "ArrowDown", "m", "z"];
        const keyPressHandler = (e: any) => {
            if(isTetrominoFalling /*&& keyUp*/) {
                if (keys.includes(e.key) && !isPaused) {
                    dispatch({type: ActionsTypes.SET_GAMEBOARD_STATE, payload: MoveTetromino(gameBoardState, e.key)});
                    //setKeyUp(false) ;
                } else if (e.key === "ArrowUp" && !isPaused) {
                    dispatch({type: ActionsTypes.SET_GAMEBOARD_STATE, payload: RotateTetromino(gameBoardState, input)});
                    //setKeyUp(false) ;

                } else if (e.key === "p") {
                    dispatch({type: ActionsTypes.SET_IS_PAUSED});
                }
            }
        };       
        window.addEventListener('keydown', keyPressHandler);
        
        return () => {
            window.removeEventListener('keydown', keyPressHandler);          
        }
    }); 

    useEffect(() => {
        const keys = ["ArrowLeft", "ArrowRight", "ArrowDown", "ArrowUp", "m", "z"];
        const keyUpHandler = (e: any) => {
            if (keys.includes(e.key)) {
                //setKeyUp(true);
            }
        }        
        window.addEventListener('keyup', keyUpHandler);        
        return () => {
            window.addEventListener('keyup', keyUpHandler);
        }
    }, [])
}