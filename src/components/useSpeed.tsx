import { useEffect } from "react";
import { ActionsTypes } from "./constants/ActionsTypes";

export const useSpeed = (linesCounter: number, speedStep: number, dispatch: (e: any) => void) => {
    useEffect(() => {
        if (linesCounter >= speedStep) {
            if(linesCounter <= 20) {
                dispatch({type: ActionsTypes.SET_SPEED, payload: (200)});
            } else if (linesCounter <= 50) {
                dispatch({type: ActionsTypes.SET_SPEED, payload: (100)});
            } else {
                dispatch({type: ActionsTypes.SET_SPEED, payload: (50)});
            }
            dispatch({type: ActionsTypes.SET_SPEED_STEP, payload: (speedStep + 10)})
        }
    }, [linesCounter, speedStep, dispatch]);
}