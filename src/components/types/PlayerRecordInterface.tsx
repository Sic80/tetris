export interface PlayerRecord {
    name: string,
    score: number
}