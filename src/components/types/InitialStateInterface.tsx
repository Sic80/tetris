import { GameBoardState } from "./GameBoardState";
import { PlayerRecord } from "./PlayerRecordInterface";
import { Tetromino } from './Tetromino';

export interface InitialState {
    gameBoardState: GameBoardState;
    isPaused: boolean; 
    speed: number; 
    speedStep: number;
    linesCounter: number;
    nextTetromino: Tetromino;
    gameTetrominoes: Tetromino[];
    isGameOver: boolean;
    isKeyUp: boolean;
    topThree: PlayerRecord[];
    topThreePosition: number;
    canGoOn: boolean;
}