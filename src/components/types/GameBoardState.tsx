import { Tetromino } from './Tetromino';

export interface GameBoardState {
    tetromino: Tetromino,
    gameBoard: (string |number)[], 
 }