export interface Tetromino {
    type: string,
    position: number[],
    id: number
}