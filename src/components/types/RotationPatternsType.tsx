export interface RotationPatterns {
    [key: string]: number[][]
  }