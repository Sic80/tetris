import { ActionsTypes } from "../constants/ActionsTypes";
import { GameBoardState } from "./GameBoardState";
import { InitialState } from "./InitialStateInterface";
import { PlayerRecord } from "./PlayerRecordInterface";

export interface setGameBoardState {
    type: typeof ActionsTypes.SET_GAMEBOARD_STATE,
    payload: GameBoardState
}

export interface setGameBoardStateAndKeyUp {
    type: typeof ActionsTypes.SET_GAMEBOARD_STATE_AND_KEY_UP,
    payload: GameBoardState
}

export interface setIsPaused {
    type: typeof ActionsTypes.SET_IS_PAUSED
}

export interface setSpeed {
    type: typeof ActionsTypes.SET_SPEED,
    payload: number
}

export interface setSpeedStep {
    type: typeof ActionsTypes.SET_SPEED_STEP,
    payload: number
}

export interface setLinesCounter {
    type: typeof ActionsTypes.SET_LINES_COUNTER,
    payload: number
}

export interface setNextTetromino {
    type: typeof ActionsTypes.SET_NEXT_TETROMINO,
}

export interface setGameTetrominoes {
    type: typeof ActionsTypes.SET_GAME_TETROMINOES,
}

export interface gameOver {
    type: typeof ActionsTypes.IS_GAME_OVER
}

export interface newGame {
    type: typeof ActionsTypes.NEW_GAME,
    payload: InitialState
}

export interface keyUp {
    type: typeof ActionsTypes.KEY_UP,
    payload: boolean
}

export interface rearrangeTopThreeAndSetPosition {
    type: typeof ActionsTypes.REARRANGE_TOP_THREE_AND_SET_POSITION,
    payload: number
}

export interface setTopThree {
    type: typeof ActionsTypes.SET_TOP_THREE,
    payload: PlayerRecord[]
}

export interface setCanGoOn {
    type: typeof ActionsTypes.SET_CAN_GO_ON,
}



export type Action = setGameBoardState | setGameBoardStateAndKeyUp | setIsPaused | setSpeed | setSpeedStep | setLinesCounter | setNextTetromino | setGameTetrominoes | gameOver | newGame | keyUp | rearrangeTopThreeAndSetPosition | setTopThree | setCanGoOn;