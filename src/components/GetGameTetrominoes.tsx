import { Tetromino } from './types/Tetromino'

export default function GetGameTetrominoes(idStart: number): Tetromino[] {

    const iTetromino: Tetromino= {type: "i", position: [14,15, 16, 17], id: -1};
    const oTetromino: Tetromino = {type: "o", position: [4, 5, 14, 15], id: -1};
    const tTetromino: Tetromino = {type: "t", position: [4, 5, 6, 15], id: -1};
    const jTetromino: Tetromino = {type: "j", position: [4, 5, 6, 16], id: -1};
    const lTetromino: Tetromino = {type: "l", position: [4, 5, 6, 14], id: -1};
    const sTetromino: Tetromino = {type: "s", position: [5, 6, 14, 15], id: -1};
    const zTetromino: Tetromino = {type: "z", position: [4, 5, 15, 16], id: -1};
    const tetrominoes: Tetromino[] = [iTetromino, oTetromino, tTetromino, jTetromino, lTetromino, sTetromino, zTetromino];

    const randomTetromino = (): number => {
        return Math.floor(Math.random() * 7);
    }

    const gameTetrominoes = () => {
        const gameTetrominoes = [];
        for (let i = 0; i < 10; i++) {
            gameTetrominoes.push({...tetrominoes[randomTetromino()], id: (i + idStart)});
        }
        return gameTetrominoes;
    }

    return gameTetrominoes();
}