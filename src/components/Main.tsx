import '../css/GameBoard.css';
import { useEffect, useReducer, useRef, useState } from 'react';
import { GameBoardState } from './types/GameBoardState';
import AutomaticMovement from './AutomaticMovement';
import useInterval from './useInterval';
import NextTetrominoDisplay from './NextTetrominoDisplay';
import { InitialState } from './types/InitialStateInterface';
import { ActionsTypes } from './constants/ActionsTypes';
import GetGameTetrominoes from './GetGameTetrominoes';
import Table from './Table';
import { reducer } from './reducer';
import { useSpeed } from './useSpeed';
import { useWindowBlur } from './useWindowBlur';
import MoveTetromino from './MoveTetromino';
import RotateTetromino from './RotateTetromino';
import Record from './Record';
import MobileButtons from './MobileButtons';


const Main = () => {
    const gameTetrominoesInit = GetGameTetrominoes(0);
    const gameBoardInitialState: GameBoardState = {tetromino: gameTetrominoesInit[0], gameBoard: new Array(220).fill(0)};
    const topThreeInitialState = [{name: ' . . . ', score: 0}, 
                                  {name: ' . . . ', score: 0}, 
                                  {name: ' . . . ', score: 0}];
    const initialState: InitialState = {
        gameBoardState: gameBoardInitialState, 
        isPaused: false, 
        speed: 1000, 
        speedStep: 10, 
        linesCounter: 0, 
        nextTetromino: gameTetrominoesInit[1], 
        gameTetrominoes: gameTetrominoesInit,
        isGameOver: false,
        isKeyUp: true,
        topThree: topThreeInitialState,
        topThreePosition: -1,
        canGoOn: true
    };
    const [state, dispatch] = useReducer(reducer, initialState);
    const { gameBoardState, isPaused, speed, speedStep, linesCounter, nextTetromino, gameTetrominoes, isGameOver, isKeyUp, topThree, topThreePosition, canGoOn } = state;     
    const latestGameBoardState = useRef(gameBoardState);
    latestGameBoardState.current = gameBoardState;    
    const  { tetromino: { position, id }, gameBoard } = latestGameBoardState.current;
    const { removedLines } = AutomaticMovement(latestGameBoardState.current, gameTetrominoes); 
    
    const nextPosition: number[] = position.map((square: number) => square + 10);
    const isPositionNotEmpty: boolean = nextPosition.filter(square => !position.includes(square)).some(square => gameBoard[square] !== 0);
    const isGameBoardFull: boolean = position.some(square => square < 19) && isPositionNotEmpty;
    const isTetrominoFalling: boolean = position.some(square => square > 19);
    const isNewTetrominoInFirstLine = isTetrominoFalling && !isPositionNotEmpty; 
    const [isRecordModalOpen, setIsRecordModalOpen] = useState<boolean>(false);
    
    useEffect(() => {
        if(isGameBoardFull) {
            dispatch({type: ActionsTypes.IS_GAME_OVER});
            setTimeout(() => {
                setIsRecordModalOpen(true);
            }, 1000);
        }
    }, [isGameBoardFull]);

    useEffect(() => {
        if ((id + 2) % 10 === 0) {
            dispatch({type: ActionsTypes.SET_GAME_TETROMINOES});
        }
    }, [id]);   
    
    useEffect(() => {
        dispatch({type: ActionsTypes.SET_NEXT_TETROMINO}); 
    }, [isNewTetrominoInFirstLine]);    
    
    useEffect(() => {
        dispatch({type: ActionsTypes.SET_LINES_COUNTER, payload: removedLines});
        dispatch({type: ActionsTypes.SET_CAN_GO_ON});
        setTimeout(() => {
            dispatch({type: ActionsTypes.SET_CAN_GO_ON});
        }, speed/2)
    }, [removedLines, speed])

    const keyAndButtonsHandler = (input: string) => {
        const keys = ["ArrowLeft", "ArrowRight", "ArrowDown", "z"];
        if (isTetrominoFalling) {
            if (keys.includes(input) && !isPaused) {
                dispatch({type: ActionsTypes.SET_GAMEBOARD_STATE_AND_KEY_UP, payload: MoveTetromino(latestGameBoardState.current, input)});
            } else if ((input === "n" || input === "m") && !isPaused) {
                dispatch({type: ActionsTypes.SET_GAMEBOARD_STATE_AND_KEY_UP, payload: RotateTetromino(latestGameBoardState.current, input)});                    
            } else if (input === "p" && !isGameOver) {
                dispatch({type: ActionsTypes.SET_IS_PAUSED});
            }
        }
    }    
    
    useEffect(() => {
        const keyDownHandler = (e: KeyboardEvent) => {
            if(isKeyUp) {
                keyAndButtonsHandler(e.key);
            }
        };       
        window.addEventListener('keydown', keyDownHandler);     
        return () => {
            window.removeEventListener('keydown', keyDownHandler);          
        }
    }); 
    
    useEffect(() => {
        const keys = ["ArrowLeft", "ArrowRight", "ArrowDown", "ArrowUp", "m", "n"];
        const keyUpHandler = (e: any) => {
            if (keys.includes(e.key)) {
                dispatch({type: ActionsTypes.KEY_UP, payload: true})
            }
        }        
        window.addEventListener('keyup', keyUpHandler);        
        return () => {
            window.removeEventListener('keyup', keyUpHandler);
        }
    }, [])
    
    useWindowBlur(isPaused, isGameOver, dispatch);
    useSpeed(linesCounter, speedStep, dispatch);    
    useInterval(() => {
        if (!isPaused && !isGameOver && canGoOn) {
            dispatch({type: ActionsTypes.SET_GAMEBOARD_STATE, payload: AutomaticMovement(latestGameBoardState.current, gameTetrominoes).gameBoardState});
        }
    }, speed);


    return ( 
        <div className="h-screen  bg-gray-300">
                {isRecordModalOpen &&
                    <div className="grid place-content-center absolute w-full md:hidden">
                        <div className="w-64 mt-24 border-8 border-gray-300 rounded-xl bg-black">
                            <Record 
                                isGameOver={isGameOver}
                                finalScore={linesCounter}
                                topThree={topThree}
                                dispatch={dispatch}
                                topThreePosition={topThreePosition}
                            />
                        </div>
                    </div>
                
                }
            <div className="grid grid-cols-4 md:grid-cols-3 h-3/5 xs:h-4/6 md:h-auto">

                <div className="col grid justify-items-end h-24 md:h-auto w-20 md:w-full">
                    <div className="bg-black text-white grid text-center w-16  h-20 md:w-40 pt-2 pb-3 rounded-md mt-2 sm:mt-20 md:h-20 mr-0 md:mr-0">
                        <p>LINES</p>                     
                        <p className=" place-self-center md:mt-2 text-base">{linesCounter}</p>
                    </div>

                    <div className="md:w-40 md:h-72 hidden md:block ml-1 md:ml-0">
                        <Record 
                            isGameOver={isGameOver}
                            finalScore={linesCounter}
                            topThree={topThree}
                            dispatch={dispatch}
                            topThreePosition={topThreePosition}
                        />
                    </div>
                </div>
                <div className="col grid justify-items-center mt-2  sm:mt-20 col-span-2 md:col-span-1 h-96 self-center">
                    <Table 
                        isPaused={isPaused}
                        isGameOver={isGameOver}
                        gameBoard={gameBoard}           
                    />
                </div>
                <div className="col grid justify-items-center mr-1 md:justify-items-start mt-2 md:mt-20 h-20 md:h-full">
                    <NextTetrominoDisplay 
                        nextTetromino ={nextTetromino}
                    />
                </div>
            </div>
                    

                
            <div className="grid h-2/5 xs:h-2/6 md:h-auto">

                <div className="self-center xs:self-start  md:mt-14 text-center">
                    {isGameOver ? 
                        <button
                            className="button h-10 w-24 rounded-lg animate-pulse place-self-center bg-black text-white"
                            onClick={() => {
                                dispatch({type: ActionsTypes.NEW_GAME, payload: initialState});
                                setIsRecordModalOpen(false);
                            }}
                        >
                            Play again
                        </button>
                        :
                        <MobileButtons keyAndButtonsHandler={keyAndButtonsHandler}/> 
                    }  
                </div>
            </div>
        </div>
     );
}
 
export default Main;