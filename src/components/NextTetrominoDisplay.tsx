import { useEffect, useReducer } from 'react';
import { DisplayState } from './types/DisplayStateInterface';
import { Tetromino } from './types/Tetromino';

export interface NextTetrominoDisplayProps {
    nextTetromino: Tetromino
}

const SET_DISPLAY_STATE = 'SET_DISPLAY_STATE';

interface SetDisplayState {
    type: typeof SET_DISPLAY_STATE
}
 
const NextTetrominoDisplay = ({ nextTetromino }: NextTetrominoDisplayProps) => {

    const { type, position } = nextTetromino;
    const hiddenCell = [0, 1, 2, 10, 11, 12, 20, 21, 22, 30, 31, 32, 9, 19, 29, 39];

    const initialState: DisplayState = {displayState: new Array(20).fill(0)};

    const [state, dispatch] = useReducer(reducer, initialState);
    const { displayState } = state;

    function reducer(state: DisplayState, action: SetDisplayState): DisplayState {
        const { displayState } = state;
        if (action.type === SET_DISPLAY_STATE) {
            const displayStateCopy: (number | string)[] = [...displayState.map(square => square = 0)];
            position.map(square => displayStateCopy[square + 10] = type);
            return {displayState: displayStateCopy};
        }
        return state;
    }    
    
    useEffect(() => {
        dispatch({type: SET_DISPLAY_STATE})
    }, [nextTetromino]);

    return (  
        <div>
            <div className="bg-black text-white text-center rounded-t-md pt-2">
                <p>
                    NEXT
                </p>
            </div>
            <table>
                <tbody>
                    {[0, 1, 2, 3].map((tr, trIdx) => 
                        <tr key={trIdx} className=" md:row-width bg-black">
                            {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((td, tdIdx) =>
                                <td
                                    key={tdIdx}
                                    className={`black-border text-center h-2 w-2 md:h-5 md:w-5 cell${trIdx-tdIdx} ${displayState[+(tr.toString() + td)]} ${hiddenCell.includes(+(tr.toString() + td)) ? "hidden" : "d-table-cell"}`}
                                ></td>     
                            )}
                        </tr>
                    )}
                </tbody>
            </table>
            <div className="bg-black h-4 rounded-b-md"></div>
        </div>
    );
}
 
export default NextTetrominoDisplay;

